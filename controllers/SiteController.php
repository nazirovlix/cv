<?php

namespace app\controllers;

use app\models\About;
use app\models\BaseModel;
use app\models\Blog;
use app\models\Category;
use app\models\Contacts;
use app\models\Experience;
use app\models\Feedback;
use app\models\Projects;
use app\models\Skills;
use app\models\Whatido;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;

class SiteController extends Controller
{

    public function init()
    {

        if (!empty(Yii::$app->request->cookies['language'])) {
            Yii::$app->language = Yii::$app->request->cookies['language'];
        } else {
            Yii::$app->language = 'en';
        }
        parent::init();
    }
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $contact = Contacts::findOne(1);
        $about = About::findOne(1);
        $what = Whatido::find()->orderBy(['order' => SORT_ASC])->all();
        $tex = Skills::find()
            ->where(['type' => Skills::TECHNICAL_SKILLS, 'status' => BaseModel::STATUS_ACTIVE])
            ->orderBy(['order' => SORT_ASC])
            ->all();
        $prof = Skills::find()
            ->where(['type' => Skills::PROFESSIONAL_SKILLS, 'status' => BaseModel::STATUS_ACTIVE])
            ->orderBy(['order' => SORT_ASC])
            ->all();
        $education = Experience::find()
            ->where(['type' => Experience::EDUCATION])
            ->orderBy(['order' => SORT_ASC])
            ->all();
        $experience = Experience::find()
            ->where(['type' => Experience::EXPERIENCE])
            ->orderBy(['order' => SORT_ASC])
            ->all();
        $blogs = Blog::find()->where(['status' => BaseModel::STATUS_ACTIVE])->all();

        $category = Category::find()->where(['status' => BaseModel::STATUS_ACTIVE])
            ->orderBy(['order' => SORT_ASC])
            ->all();
        $project = Projects::find()->where(['status' => BaseModel::STATUS_ACTIVE])
            ->orderBy(['order' => SORT_ASC])
            ->all();

        return $this->render('index',[
            'contact' => $contact,
            'about' => $about,
            'what' => $what,
            'tex' => $tex,
            'prof' => $prof,
            'education' => $education,
            'experience' => $experience,
            'blogs' => $blogs,
            'category' => $category,
            'project' => $project
        ]);
    }


    public function actionFeedback()
    {
        $feed = new Feedback();
        $feed->fullname = Yii::$app->request->get('fullname');
        $feed->company = Yii::$app->request->get('company');
        $feed->email = Yii::$app->request->get('email');
        $feed->text = Yii::$app->request->get('text');
        if ($feed->save()) {
            return true;
        }
        return false;
    }


    public function actionProject($id)
    {
        $project = Projects::findOne($id);
        if($project){
            return $this->renderAjax('projectModal',[
                'project' => $project
            ]);
        }

        return false;
    }

    /**
     *  Change Language
     * @param $lang
     * @return \yii\web\Response
     */
    public function actionSetlang($lang)
    {
        $langs = ['en', 'ru'];
        if (in_array($lang, $langs)) {
            Yii::$app->language = $lang;
            Yii::$app->session->set('app_lang', $lang);
            $cookies = Yii::$app->response->cookies;
            $cookies->add(new \yii\web\Cookie([
                'name' => 'language',
                'value' => $lang,
            ]));
        }
        return $this->redirect(Yii::$app->request->referrer);
    }

}
