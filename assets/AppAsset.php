<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'libs/font-awesome-4.7.0/css/font-awesome.min.css',
        'libs/bootstrap-4.3.1-dist/css/bootstrap.min.css',
        'libs/animate/animate.css',
        'libs/OwlCarousel2-2.3.4/dist/assets/owl.carousel.min.css',
        'libs/fancybox-master/dist/jquery.fancybox.min.css',
        'css/style.css',
        'css/media.css',
        'css/site.css',
    ];
    public $js = [
        'js/jquery.min.js',
        'js/popper.js',
        'libs/bootstrap-4.3.1-dist/js/bootstrap.min.js',
        'libs/OwlCarousel2-2.3.4/dist/owl.carousel.min.js',
        'js/validator.js',
        'js/wow.js',
        'js/mixitup.js',
        '/js/circle-progress.js',
        'js/nav.js',
        'libs/fancybox-master/dist/jquery.fancybox.min.js',
        'js/isotope.js',
        'js/packery.js',
        'js/custom.js',
        'js/sweet.js',
        'js/main.js'
    ];
    public $depends = [
//        'yii\web\YiiAsset',
//        'yii\bootstrap\BootstrapAsset',
    ];
}
