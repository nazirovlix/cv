<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%contacts}}`.
 */
class m190413_181641_create_contacts_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%contacts}}', [
            'id' => $this->primaryKey(),
            'image' => $this->string(),
            'fullname_ru' => $this->string(),
            'fullname_en' => $this->string(),
            'email' => $this->string(),
            'phone' => $this->string(),
            'address_ru' => $this->string(),
            'address_en' => $this->string(),
            'guthub_url' => $this->string(),
            'bitbucket_url' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%contacts}}');
    }
}
