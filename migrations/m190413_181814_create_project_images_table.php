<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%project_images}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%projects}}`
 */
class m190413_181814_create_project_images_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%project_images}}', [
            'id' => $this->primaryKey(),
            'project_id' => $this->integer()->notNull(),
            'image' => $this->string(),
            'content_ru' => $this->text(),
            'content_en' => $this->text(),
        ]);

        // creates index for column `project_id`
        $this->createIndex(
            '{{%idx-project_images-project_id}}',
            '{{%project_images}}',
            'project_id'
        );

        // add foreign key for table `{{%projects}}`
        $this->addForeignKey(
            '{{%fk-project_images-project_id}}',
            '{{%project_images}}',
            'project_id',
            '{{%projects}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%projects}}`
        $this->dropForeignKey(
            '{{%fk-project_images-project_id}}',
            '{{%project_images}}'
        );

        // drops index for column `project_id`
        $this->dropIndex(
            '{{%idx-project_images-project_id}}',
            '{{%project_images}}'
        );

        $this->dropTable('{{%project_images}}');
    }
}
