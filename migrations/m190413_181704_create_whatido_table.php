<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%whatido}}`.
 */
class m190413_181704_create_whatido_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%whatido}}', [
            'id' => $this->primaryKey(),
            'title_ru' => $this->string(),
            'title_en' => $this->string(),
            'content_ru' => $this->text(),
            'content_en' => $this->text(),
            'order' => $this->smallInteger(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%whatido}}');
    }
}
