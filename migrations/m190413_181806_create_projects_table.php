<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%projects}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%category}}`
 */
class m190413_181806_create_projects_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%projects}}', [
            'id' => $this->primaryKey(),
            'category_id' => $this->integer()->notNull(),
            'image' => $this->string(),
            'name_ru' => $this->string(),
            'name_en' => $this->string(),
            'content_ru' => $this->text(),
            'content_en' => $this->text(),
            'skills' => $this->string(),
            'url' => $this->string(),
            'order' => $this->integer(),
            'status' => $this->integer(),
        ]);

        // creates index for column `category_id`
        $this->createIndex(
            '{{%idx-projects-category_id}}',
            '{{%projects}}',
            'category_id'
        );

        // add foreign key for table `{{%category}}`
        $this->addForeignKey(
            '{{%fk-projects-category_id}}',
            '{{%projects}}',
            'category_id',
            '{{%category}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%category}}`
        $this->dropForeignKey(
            '{{%fk-projects-category_id}}',
            '{{%projects}}'
        );

        // drops index for column `category_id`
        $this->dropIndex(
            '{{%idx-projects-category_id}}',
            '{{%projects}}'
        );

        $this->dropTable('{{%projects}}');
    }
}
