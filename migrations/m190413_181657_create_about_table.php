<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%about}}`.
 */
class m190413_181657_create_about_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%about}}', [
            'id' => $this->primaryKey(),
            'image' => $this->string(),
            'content_ru' => $this->text(),
            'content_en' => $this->text(),
            'skills' => $this->text(),
            'cv' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%about}}');
    }
}
