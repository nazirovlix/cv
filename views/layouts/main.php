<?php

/* @var $this \yii\web\View */

/* @var $content string */

use app\models\Contacts;
use app\models\Feedback;
use yii\helpers\Html;
use app\assets\AppAsset;
use yii\helpers\Url;

AppAsset::register($this);
$contacts = Contacts::findOne(1);
$feedback = new Feedback();

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- FAV AND ICONS   -->
    <link rel="shortcut icon" href="/images/favicon.ico">
    <link rel="shortcut icon" href="/images/apple-icon.png">
    <link rel="shortcut icon" sizes="72x72" href="/images/apple-icon-72x72.png">
    <link rel="shortcut icon" sizes="114x114" href="/images/apple-icon-114x114.png">
    <!-- Google Font-->
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Yii::t('app','Nazirov Khasanjon | Web developer') ?></title>
    <?php $this->head() ?>
</head>
<body class="dark-vertion black-bg">
<!-- Start Loader -->
<div class="section-loader" style="display: none">
    <div class="loader">
        <div></div>
        <div></div>
    </div>
</div>
<!-- End Loader -->
<!--
===================
   NAVIGATION
===================
-->
<header class="black-bg mh-header mh-fixed-nav nav-scroll mh-xs-mobile-nav" id="mh-header">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <nav class="navbar navbar-expand-lg mh-nav nav-btn">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto ml-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="#mh-home"><?= Yii::t('app','Home') ?></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#mh-about"><?= Yii::t('app','About') ?></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#mh-skills"><?= Yii::t('app','Skills') ?></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#mh-experience"><?= Yii::t('app','Experiences') ?></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#mh-portfolio"><?= Yii::t('app','Portfolio') ?></a>
                        </li>
<!--                        <li class="nav-item">-->
<!--                            <a class="nav-link" href="#mh-blog">--><?//= Yii::t('app','Blog') ?><!--</a>-->
<!--                        </li>-->
                        <li class="nav-item">
                            <a class="nav-link" href="#mh-contact"><?= Yii::t('app','Contact') ?></a>
                        </li>
                    </ul>
                    <ul class="navbar-nav">
                        <li class="language <?= Yii::$app->language == 'ru' ? 'active' : '' ?>">
                            <a href="<?= Url::to(['/site/setlang','lang' => 'ru']) ?>">RU</a>
                        </li>
                        <li class="language <?= Yii::$app->language == 'en' ? 'active' : '' ?>">
                            <a href="<?= Url::to(['/site/setlang','lang' => 'en']) ?>">EN</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
</header>



<?php $this->beginBody() ?>
        <?= $content ?>
<?php $this->endBody() ?>
<!--
===================
   FOOTER 3
===================
-->
<footer class="mh-footer mh-footer-3" id="mh-contact">
    <div class="container-fluid">
        <div class="row section-separator">
            <div class="col-sm-12 section-title wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.2s">
                <h3><?= Yii::t('app','Contact Me') ?></h3>
            </div>
            <div class="map-image image-bg col-sm-12">
                <div class="container mt-30">
                    <div class="row">
                        <div class="col-sm-12 col-md-6 mh-footer-address">
                            <div class="col-sm-12 xs-no-padding">
                                <div class="mh-address-footer-item dark-bg shadow-1 media wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.2s">
                                    <div class="each-icon">
                                        <i class="fa fa-github"></i>
                                    </div>
                                    <div class="each-info media-body">
                                        <h4><?= Yii::t('app','VCS') ?></h4>
                                        <address>
                                            GitHub - <a href="<?= !empty($contacts->guthub_url) ? $contacts->guthub_url : ''  ?>"><?= Yii::t('app','/mrlix') ?></a><br>
                                            Bitbucket - <a href="<?= !empty($contacts->bitbucket_url) ? $contacts->bitbucket_url : ''  ?>"><?= Yii::t('app','/nazirovlix') ?></a><br>
                                        </address>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 xs-no-padding">
                                <div class="mh-address-footer-item media dark-bg shadow-1 wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.4s">
                                    <div class="each-icon">
                                        <i class="fa fa-envelope-o"></i>
                                    </div>
                                    <div class="each-info media-body">
                                        <h4>Email</h4>
                                        <a href="mailto:<?= !empty($contacts->email) ? $contacts->email : ''  ?>"><?= !empty($contacts->email) ? $contacts->email : ''  ?></a><br>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 xs-no-padding">
                                <div class="mh-address-footer-item media dark-bg shadow-1 wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.6s">
                                    <div class="each-icon">
                                        <i class="fa fa-phone"></i>
                                    </div>
                                    <div class="each-info media-body">
                                        <h4><?= Yii::t('app','Phone') ?></h4>
                                        <a href="callto:<?= !empty($contacts->phone) ? strip_tags($contacts->phone) : ''  ?>"><?= !empty($contacts->phone) ? $contacts->phone : ''  ?></a><br>
                                        <?= Yii::t('app','Telegram') ?>: <a href="<?= !empty($contacts->telegram_url) ? strip_tags($contacts->telegram_url) : ''  ?>"><?= Yii::t('app','/tbotuz')  ?></a><br>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6 wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.2s">
                            <form id="contactForm" class="single-form quate-form wow fadeInUp" data-toggle="validator" method="get">
                                <div id="msgSubmit" class="h3 text-center hidden"></div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <input name="fullname" class="contact-name form-control" id="name" type="text" placeholder="<?= Yii::t('app','Name') ?>" required>
                                    </div>

                                    <div class="col-sm-12">
                                        <input name="company" class="contact-email form-control" id="L_name" type="text" placeholder="<?= Yii::t('app','Company') ?>">
                                    </div>

                                    <div class="col-sm-12">
                                        <input name="email" class="contact-subject form-control" id="email" type="email" placeholder="<?= Yii::t('app','Email') ?>">
                                    </div>

                                    <div class="col-sm-12">
                                        <textarea name="text" class="contact-message" id="message" rows="6" placeholder="<?= Yii::t('app','Your Message') ?>" required></textarea>
                                    </div>

                                    <!-- Subject Button -->
                                    <div class="btn-form col-sm-12">
                                        <button type="submit" class="btn btn-fill btn-block" id="form-submit"><?= Yii::t('app','Send Message') ?></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-sm-12 mh-copyright wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.2s">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="text-left text-xs-center">
                                        <p><?= Yii::t('app','All right reserved')?> | <?= Yii::t('app','Nairov Khasanjon')?> | 2018-2019</p>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <ul class="social-icon">
                                        <li><a href="<?= !empty($contacts->guthub_url) ? $contacts->guthub_url : ''  ?>"><i class="fa fa-github"></i></a></li>
                                        <li><a href="<?= !empty($contacts->bitbucket_url) ? $contacts->bitbucket_url : ''  ?>"><i class="fa fa-bitbucket"></i></a></li>
                                        <li><a href="<?= !empty($contacts->telegram_url) ? $contacts->telegram_url : ''  ?>"><i class="fa fa-telegram"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>


</body>
</html>
<?php $this->endPage() ?>
