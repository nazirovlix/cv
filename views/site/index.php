<?php

use yii\helpers\Json;

?>
    <!--
    ===================
       Home
    ===================
    -->
    <section class="mh-home image-bg home-2-img" id="mh-home">
        <div class="img-foverlay img-color-overlay">
            <div class="container">
                <div class="row section-separator xs-column-reverse vertical-middle-content home-padding">
                    <div class="col-sm-6">
                        <div class="mh-header-info">
                            <div class="mh-promo wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.1s">
                                <span><?= Yii::t('app', 'Hello I\'m') ?></span>
                            </div>

                            <h2 class="wow fadeInUp" data-wow-duration="0.8s"
                                data-wow-delay="0.2s"><?= !empty($contact->fullname) ? $contact->fullname : '' ?></h2>
                            <h4 class="wow fadeInUp" data-wow-duration="0.8s"
                                data-wow-delay="0.3s"><?= Yii::t('app', 'Web Developer') ?></h4>

                            <ul>
                                <li class="wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.4s"><i
                                            class="fa fa-envelope"></i><a
                                            href="mailto:<?= !empty($contact->email) ? $contact->email : '' ?>"><?= !empty($contact->email) ? $contact->email : '' ?></a>
                                </li>
                                <li class="wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.5s"><i
                                            class="fa fa-phone"></i><a
                                            href="callto:<?= !empty($contact->phone) ? str_replace(' ', '', $contact->phone) : '' ?>"><?= !empty($contact->phone) ? $contact->phone : '' ?></a>
                                </li>
                                <li class="wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.6s"><i
                                            class="fa fa-map-marker"></i>
                                    <address><?= !empty($contact->address) ? $contact->address : '' ?></address>
                                </li>
                            </ul>

                            <ul class="social-icon wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.7s">
                                <li><a href="<?= !empty($contact->guthub_url) ? $contact->guthub_url : '' ?>"><i
                                                class="fa fa-github"></i></a></li>
                                <li><a href="<?= !empty($contact->bitbucket_url) ? $contact->bitbucket_url : '' ?>"><i
                                                class="fa fa-bitbucket"></i></a></li>
                                <li><a href="<?= !empty($contact->telegram_url) ? $contact->telegram_url : '' ?>"><i
                                                class="fa fa-telegram"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="hero-img wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.6s">
                            <div class="img-border">
                                <img src="<?= !empty($contact->image) ? '/uploads/' . $contact->image : 'assets/images/hero.png' ?>"
                                     alt="" class="img-fluid">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--
    ===================
       ABOUT
    ===================
    -->
    <section class="mh-about" id="mh-about">
        <div class="container">
            <div class="row section-separator">
                <div class="col-sm-12 col-md-6">
                    <div class="mh-about-img shadow-2 wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.4s">
                        <img src="/uploads/<?= $about->image ?>" alt="" class="img-fluid">
                    </div>
                </div>
                <div class="col-sm-12 col-md-6">
                    <div class="mh-about-inner">
                        <h2 class="wow fadeInUp" data-wow-duration="0.8s"
                            data-wow-delay="0.1s"><?= Yii::t('app', 'About Me') ?></h2>
                        <p class="wow fadeInUp" data-wow-duration="0.8s"
                           data-wow-delay="0.2s"><?= strip_tags($about->content) ?></p>
                        <div class="mh-about-tag wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.3s">
                            <ul>
                                <?php
                                $decode = Json::decode($about->skills);

                                foreach ($decode as $item) {
                                    echo '<li><span>' . $item . '</span></li>';
                                }
                                ?>
                            </ul>
                        </div>
                        <a href="/uploads/<?= $about->cv ?>" class="btn btn-fill wow fadeInUp"
                           download="<?= $about->cv ?>"
                           data-wow-duration="0.8s" data-wow-delay="0.4s">
                            <?= Yii::t('app','Download CV') ?><i class="fa fa-download"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--
    ===================
       SERVICE
    ===================
    -->
<?php if (!empty($what)): ?>
    <section class="mh-service">
        <div class="container">
            <div class="row section-separator">
                <div class="col-sm-12 text-center section-title wow fadeInUp" data-wow-duration="0.8s"
                     data-wow-delay="0.2s">
                    <h2><?= Yii::t('app', 'What I do') ?></h2>
                </div>

                <?php foreach ($what as $item) : ?>

                    <div class="col-sm-4">
                        <div class="mh-service-item shadow-1 dark-bg wow fadeInUp" data-wow-duration="0.8s"
                             data-wow-delay="0.7s">
                            <i class="fa fa-object-ungroup sky-color"></i>
                            <h3><?= strip_tags($item->title) ?></h3>
                            <p>
                                <?= strip_tags($item->content) ?>
                            </p>
                        </div>
                    </div>

                <?php endforeach; ?>
            </div>
        </div>
    </section>
<?php endif; ?>
    <!--
    ===================
       SKILLS
    ===================
    -->
    <section class="mh-skills" id="mh-skills">
        <div class="home-v-img">
            <div class="container">
                <div class="row section-separator">
                    <div class="section-title text-center col-sm-12">
                        <!--<h2>Skills</h2>-->
                    </div>
                    <div class="col-sm-12 col-md-6">
                        <div class="mh-skills-inner">
                            <div class="mh-professional-skill wow fadeInUp" data-wow-duration="0.8s"
                                 data-wow-delay="0.3s">
                                <h3><?= Yii::t('app', 'Technical Skills') ?></h3>
                                <div class="each-skills">

                                    <?php foreach ($tex as $item) : ?>
                                        <div class="candidatos">
                                            <div class="parcial">
                                                <div class="info">
                                                    <div class="nome"><?= $item->name ?></div>
                                                    <div class="percentagem-num"><?= $item->present ?>%</div>
                                                </div>
                                                <div class="progressBar">
                                                    <div class="percentagem"
                                                         style="width: <?= $item->present ?>%;"></div>
                                                </div>
                                            </div>
                                        </div>
                                    <? endforeach; ?>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6">
                        <div class="mh-professional-skills wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.5s">
                            <h3><?= Yii::t('app','Professional Skills') ?></h3>
                            <ul class="mh-professional-progress">

                                <?php foreach ($prof as $item): ?>
                                    <li>
                                        <div class="mh-progress mh-progress-circle"
                                             data-progress="<?= $item->present ?>"></div>
                                        <div class="pr-skill-name"><?= $item->name ?></div>
                                    </li>
                                <?php endforeach; ?>

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--
    ===================
       EXPERIENCES
    ===================
    -->
    <section class="mh-experince image-bg featured-img-one" id="mh-experience">
        <div class="img-color-overlay">
            <div class="container">
                <div class="row section-separator">
                    <div class="col-sm-12 col-md-6">
                        <div class="mh-education">
                            <h3 class="wow fadeInUp" data-wow-duration="0.8s"
                                data-wow-delay="0.2s"><?= Yii::t('app', 'Education') ?></h3>
                            <div class="mh-education-deatils">
                                <!-- Education Institutes-->

                                <?php foreach ($experience as $item): ?>
                                    <div class="mh-education-item dark-bg wow fadeInUp" data-wow-duration="0.8s"
                                         data-wow-delay="0.3s">
                                        <h4><?= $item->title ?> <a href=""><?= $item->company ?></a></h4>
                                        <div class="mh-eduyear"><?= $item->from_to ?></div>
                                        <p><?= $item->content ?> </p>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6">
                        <div class="mh-work">
                            <h3>Work Experience</h3>
                            <div class="mh-experience-deatils">
                                <!-- Education Institutes-->

                                <?php foreach ($education as $item): ?>
                                    <div class="mh-work-item dark-bg wow fadeInUp" data-wow-duration="0.8s"
                                         data-wow-delay="0.4s">
                                        <h4><?= $item->title ?> <a href=""><?= $item->company ?></a></h4>
                                        <div class="mh-eduyear"><?= $item->from_to ?></div>
                                        <ul class="work-responsibility">
                                            <li><?= $item->content ?></li>
                                        </ul>
                                    </div>

                                <?php endforeach; ?>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--
    ===================
       PORTFOLIO
    ===================
    -->
    <section class="mh-portfolio" id="mh-portfolio">
        <div class="container">
            <div class="row section-separator">
                <div class="section-title col-sm-12 wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.1s">
                    <h3>Recent Portfolio</h3>
                </div>
                <div class="part col-sm-12">
                    <div class="portfolio-nav col-sm-12" id="filter-button">
                        <ul>
                            <li data-filter="*" class="current wow fadeInUp" data-wow-duration="0.8s"
                                data-wow-delay="0.1s">
                                <span>All Categories</span></li>


                            <?php foreach ($category as $item): ?>

                                <li data-filter=".<?= $item->id ?>" class="wow fadeInUp" data-wow-duration="0.8s"
                                    data-wow-delay="0.2s"><span><?= $item->name ?></span></li>

                            <?php endforeach; ?>
                        </ul>
                    </div>
                    <div class="mh-project-gallery col-sm-12 wow fadeInUp" id="project-gallery" data-wow-duration="0.8s"
                         data-wow-delay="0.5s">
                        <div class="portfolioContainer row">

                            <?php foreach ($project as $item): ?>

                                <div class="grid-item col-md-4 col-sm-6 col-xs-12 ui <?= $item->category_id ?>">
                                    <figure>
                                        <img src="/uploads/<?= $item->image ?>" alt="<?= $item->name ?>">
                                        <figcaption class="fig-caption">
                                            <i class="fa fa-search"></i>
                                            <h5 class="title"><?= $item->name ?></h5>
                                            <span class="sub-title"><?= $item->content ?></span>
                                            <a href="#" data-fancybox data-src="#mh" class="projectModal" data-id="<?= $item->id ?>"></a>
                                        </figcaption>
                                    </figure>
                                </div>
                            <?php endforeach; ?>

                        </div> <!-- End: .grid .project-gallery -->
                    </div> <!-- End: .grid .project-gallery -->
                </div> <!-- End: .part -->
            </div> <!-- End: .row -->
        </div>


        <div class="mh-portfolio-modal" id="mh">

        </div>

    </section>

    <!--
    ===================
       BLOG
    ===================
    -->
<?php if (!empty($blogs)): ?>

    <section class="mh-blog image-bg featured-img-two" id="mh-blog">
        <div class="img-color-overlay">
            <div class="container">
                <div class="row section-separator">
                    <div class="col-sm-12 section-title wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.2s">
                        <h3>Featured Posts</h3>
                    </div>
                    <?php foreach ($blogs as $item): ?>
                        <div class="col-sm-12 col-md-4">
                            <div class="mh-blog-item dark-bg wow fadeInUp" data-wow-duration="0.8s"
                                 data-wow-delay="0.3s">
                                <img src="assets/images/b-3.png" alt="" class="img-fluid">
                                <div class="blog-inner">
                                    <h2><a href="blog-single.html?id=<?= $item->id ?>"><?= $item->title ?></a></h2>
                                    <div class="mh-blog-post-info">
                                        <ul>
                                            <li><strong><?= Yii::t('app', 'Post On') ?></strong><a
                                                        href=""><?= date('m/d/Y', $item->created_at); ?></a></li>
                                        </ul>
                                    </div>
                                    <p><?= $item->content ?></p>
                                    <a href="blog-single.html?id=<?= $item->id ?>">Read More</a>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>