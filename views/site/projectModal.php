<?php
/**
 * Created by PhpStorm.
 * User: Bakhrom Abdukodirov
 * Date: 26.04.2019
 * Time: 10:32
 */

?>
<div class="container">
    <div class="row mh-portfolio-modal-inner">
        <div class="col-sm-5">
            <h2><?= $project->name ?></h2>
            <p><?= $project->content ?></p>
            <div class="mh-about-tag">
                <ul>
                    <?php $skills = \yii\helpers\Json::decode($project->skills); ?>
                    <?php if ($skills): ?>
                        <?php foreach ($skills as $item): ?>
                            <li><span><?= $item ?></span></li>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </ul>
            </div>
            <a href="<?= $project->url ?>" target="_blank" class="btn btn-fill"><?= Yii::t('app', 'Open the project') ?></a>
        </div>
        <div class="col-sm-7">
            <div class="mh-portfolio-modal-img">
                <?php if ($project->projectImages): ?>
                    <?php foreach ($project->projectImages as $item): ?>
                        <img src="/uploads/<?= $item->image ?>" alt="" class="img-fluid">
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
