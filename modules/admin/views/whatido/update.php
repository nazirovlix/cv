<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Whatido */

$this->title = 'Update Whatido: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Whatidos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="whatido-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
