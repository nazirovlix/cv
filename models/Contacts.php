<?php

namespace app\models;

use trntv\filekit\behaviors\UploadBehavior;
use Yii;

/**
 * This is the model class for table "contacts".
 *
 * @property int $id
 * @property string $image
 * @property string $fullname_ru
 * @property string $fullname_en
 * @property string $email
 * @property string $phone
 * @property string $address_ru
 * @property string $address_en
 * @property string $guthub_url
 * @property string $telegram_url
 * @property string $bitbucket_url
 */
class Contacts extends \yii\db\ActiveRecord
{
    public $base_file;
    /**
     * @return array
     */
    public function behaviors ()
    {
        return [
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'base_file',
                'pathAttribute' => 'image',
                'baseUrlAttribute' => false
            ]
        ];
    }
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'contacts';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['image', 'fullname_ru', 'fullname_en', 'telegram_url', 'email', 'phone', 'address_ru', 'address_en', 'guthub_url', 'bitbucket_url'], 'string', 'max' => 255],
            [['base_file'],'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'image' => 'Image',
            'base_file' => 'Image',
            'fullname_ru' => 'Fullname Ru',
            'fullname_en' => 'Fullname En',
            'email' => 'Email',
            'phone' => 'Phone',
            'address_ru' => 'Address Ru',
            'address_en' => 'Address En',
            'guthub_url' => 'Guthub Url',
            'bitbucket_url' => 'Bitbucket Url',
        ];
    }

    public function getFullname()
    {
        return $this->{'fullname_'. Yii::$app->language};
    }

    public function getAddress()
    {
        return $this->{'address_'. Yii::$app->language};
    }
}
