<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "whatido".
 *
 * @property int $id
 * @property string $title_ru
 * @property string $title_en
 * @property string $content_ru
 * @property string $content_en
 * @property int $order
 */
class Whatido extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'whatido';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['content_ru', 'content_en'], 'string'],
            [['order'], 'integer'],
            [['title_ru', 'title_en'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title_ru' => 'Title Ru',
            'title_en' => 'Title En',
            'content_ru' => 'Content Ru',
            'content_en' => 'Content En',
            'order' => 'Order',
        ];
    }
    public function getTitle(){
        return $this->{'title_'.Yii::$app->language};
    }
    public function getContent(){
        return $this->{'content_'.Yii::$app->language};
    }
}

