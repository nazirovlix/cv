<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "skills".
 *
 * @property int $id
 * @property string $name_ru
 * @property string $name_en
 * @property int $present
 * @property int $type
 * @property int $order
 * @property int $status
 */
class Skills extends \yii\db\ActiveRecord
{

    const TECHNICAL_SKILLS = 1;
    const PROFESSIONAL_SKILLS = 2;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'skills';
    }


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['present', 'type', 'order', 'status'], 'integer'],
            [['name_ru', 'name_en'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name_ru' => 'Name Ru',
            'name_en' => 'Name En',
            'present' => 'Present',
            'type' => 'Type',
            'order' => 'Order',
            'status' => 'Status',
        ];
    }

    public function getName()
    {
        return $this->{'name_' . Yii::$app->language};
    }


}
