<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "experience".
 *
 * @property int $id
 * @property string $title_ru
 * @property string $title_en
 * @property string $company
 * @property string $from_to
 * @property string $content_ru
 * @property string $content_en
 * @property int $order
 */
class Experience extends \yii\db\ActiveRecord
{
    const EDUCATION = 1;
    const EXPERIENCE = 2;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'experience';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['content_ru', 'content_en'], 'string'],
            [['order','type'], 'integer'],
            [['title_ru', 'title_en', 'company', 'from_to'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title_ru' => 'Title Ru',
            'title_en' => 'Title En',
            'company' => 'Company',
            'from_to' => 'From To',
            'content_ru' => 'Content Ru',
            'content_en' => 'Content En',
            'order' => 'Order',
            'type' => 'Type',
        ];
    }
    public function getTitle(){
        return $this->{'title_'. Yii::$app->language};
    }

    public function getContent(){
        return $this->{'content_'. Yii::$app->language};
    }
}
