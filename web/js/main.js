$(document).on('submit', '#contactForm', function (e) {
    e.preventDefault();
    var form = $(this);
    $.ajax({
        url: '/site/feedback',
        type: form.attr('method'), // GET
        data: form.serialize(),    //
        success: function (res) {
            if (res == true) {
                swal("Спасибо!", "Мы свяжемся с вами в ближайшее время", "success");
                $('#contactForm')[0].reset()
            } else {
                swal("Ошибка!", "Пожалуйста, заполните все поля правильно", "warning");
            }
        },
        error: function () {
            swal("Ошибка!", "", "warning");
        }
    })
})

// project modal
$(document).on('click', '.projectModal', function () {

    var idddd = $(this).data('id');

    $.ajax({
        url: '/site/project',
        type: 'get',
        data: {id: idddd},
        success: function (res) {
            if (res !== false)
                $('.mh-portfolio-modal').html(res);
        },
        error: function () {
            swal("Ошибка!", "", "warning");
        }
    })
});
